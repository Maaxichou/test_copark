<?php

namespace App\Controller;

use App\Entity\Parking;
use App\Form\ParkingType;
use App\Repository\ParkingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class ParkingListController extends AbstractController
{
    /**
     * Fonction qui affiche la liste de tout les parkings
     * @Route("/", name="parking_list")
     */
    public function index(ParkingRepository $repo)
    {
        $parkings = $repo->findBy([]);
        return $this->render('parking/index.html.twig', [
            'controller_name' => 'ParkingListController',
            'parkings' => $parkings
        ]);
    }

    /**
     * Fonction qui affiche un seul parking en fonction de son ID
     * @Route("/search/{id}", name="one_parking")
     */
    public function one(ParkingRepository $repo, int $id)
    {
        $parking = $repo->find($id);
        return $this->render('parking/one_parking.html.twig', [
            'controller_name' => 'ParkingListController',
            'parking' => $parking
        ]);
    }

    /**
     * Fonction qui ajoute le parking dans la BDD et permet d'afficher le formulaire dans le template
     * @Route("/add", name="add_parking")
     */
    public function add(Request $request, EntityManagerInterface $em)
    {
        $parking = new Parking();
        $form = $this->createForm(ParkingType::class, $parking);
        $form->handleRequest($request);

        if ($form->isSubmitted()  && $form->isValid()) {
            $em->persist($parking);
            $em->flush();
        }

        return $this->render('parking/add_parking.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
