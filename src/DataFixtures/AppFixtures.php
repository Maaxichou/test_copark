<?php

namespace App\DataFixtures;

use App\Entity\Parking;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Migrations\Version\Factory;

class AppFixtures extends Fixture
{
    /**
     * Jeu de données 
     */
    public function load(ObjectManager $manager)
    {
    
        $manager->flush();
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 30; $i++) {
            $parking = new Parking();
            $parking->setName('Parking ' . $faker->company)
                ->setAdress(random_int(1, 150) . ' ' . $faker->streetName)
                ->setCountry($faker->country)
                ->setPostcode(intval($faker->postcode))
                ->setLatitude($faker->latitude . ' E')
                ->setLongitude($faker->longitude . ' N');
            $manager->persist($parking);
            $manager->flush();
        }
    }
}
